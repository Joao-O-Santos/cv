# About Me

-----------------------------------------------------------------------

<p style="text-align: left; padding-left: 0; text-indent: 0%">
<b>I'm João (John), a teaching assistant with a PhD in social
psychology, a part-time consultant on research methods, statistics and R
programming, and a full time Linux geek. I'm fascinated by people,
science, and technology.</b></p>




# Projects

-----------------------------------------------------------------------

2024-Present: Maintainer of the
[`tr.iatgen` `R` package](https://github.com/iatgen/tr.iatgen)
(also contributed to the
[`iatgen` R package](https://github.com/iatgen/iatgen)).

2021-Present: Helped establish and coordinate an `R` user group named
[RUGGED](https://rggd.gitlab.io). I'm also spearheading the development
of the [group's projects](https://rggd.gitlab.io/projects.html), namely:

- MOC: An ongoing effort to build a *Medium Online Course* on using
  `R` for data analysis in psychology and related fields. [The main
  textbook is available online for free](https://rggd.gitlab.io/moc),
  being automatically rendered using `R/bookdown` and `GitLab's CI/CD`.

- SciOps: An attempt to develop a user-friendly template to help
  researchers leverage DevOps tools (`git`, `Docker`, `GitLab`) to
  increase scientific productivity, reproducibility, and transparency.

- RUGGED's website: The group's official
  [website](https://rggd.gitlab.io), rendered using [make
  it_stop](https://gitlab.com/joao-o-santos/make-it-stop), a static
  website generator I'm developing.

2021-2023: SPGrow 360 – collaborated with Sérgio Moreira in
developing an R/shiny application to automatically generate 360 (i.e.,
self- and peer-evaluation) evaluation reports of workers' social
performance.

2022-2023: QHELP - worked with and mentored students, under the
supervision of Sérgio Moreira, to build `R/Shiny` apps for teaching
about linear models and their assumptions. I also presented the SciOps
projects, and mentored two students who contributed to it.

2017-2021: Collaborated in developing and maintaining LiSP’s PhD program
official website.

**Note: To see a more detailed list of my projects please checkout my
[portfolio](https://joao-o-santos.gitlab.io/portfolio.html), and/or my
[GitLab profile](https://gitlab.com/joao-o-santos).**



# Skills

-----------------------------------------------------------------------

Statistical analyzes: `R` (including: generalized mixed effects models,
robust statistics, basic simulation techniques, graphics with `ggplot2`,
automatically generating reports using `RMarkdown`, and developing
interactive `R/Shiny` web apps).

Programming: `C`, `R`, `POSIX shell`, also dabbled in `Python`, `Lua`
and `SQL`.

Tools: `Linux` (proficient user), `git`, `make`, `sed` as well as other
`coreutils`; `GitHub` and `Gitlab` (including `gitlab CI/CD`).

Web: `html`, `css`, interactive `R/Shiny` applications.

Experiment Programming: `Qualtrics`, `Iatgen`, `OpenSesame`, `Mouse
Tracker`.

Open Science: Preregistration and data sharing using
[osf.io](https://osf.io) and [aspredicted.org](https://aspredicted.org).

Markup: `Markdown` (including `RMarkdown` and `Bookdown`), `LaTeX`, as
well as multi-format conversion using `pandoc` (including custom
templates).




# Education

-----------------------------------------------------------------------

PhD in Social Psychology (2024), MSc. in Applied Social Cognition
(2017), and a BSc. in Psychological Sciences (2015), University of
Lisbon - Department of Psychology.




# Languages

-----------------------------------------------------------------------

Portuguese (native speaker)

English (fluent)

Spanish (basic comprehension)

French (basic comprehension)

Latin (very basic comprehension, personal hobby)

-----------------------------------------------------------------------
