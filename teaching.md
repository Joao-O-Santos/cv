# About Me

-----------------------------------------------------------------------

<p style="text-align: left; padding-left: 0; text-indent: 0%">
<b>I'm João (John), I have a great interest in connecting philosophy of
science, research methods, and data analysis.</b><br>My research has
focused on how adults perceive children, as a social category. My
passion for programming and IT has led me to my freelance work, where
I leverage technology to solve problems and automate tasks, in academia
and in the industry.</p>




# Current Positions

-----------------------------------------------------------------------

**Teaching Assistant** at ISPA (Instituto Universitário; Lisbon)

**Consultant and Tutor**, Remote or Hybrid (Lisbon, Portugal)




# Education

-----------------------------------------------------------------------

PhD (2024), Social Psychology, University of Lisbon - Department
of Psychology.

MSc. (2017), Applied Social Cognition, University of Lisbon - Department
of Psychology.

BSc. (2015), Psychological Sciences, University of Lisbon - Department
of Psychology.




# Theses

-----------------------------------------------------------------------

Santos, J. O. (2024). *"You're too young to understand": Investigating childist
beliefs and practices*. (Unpublished doctoral thesis). University of
Lisbon - Department of Psychology, Portugal.

Santos, J. O. (2017). *Do we infrahumanize our children?*. (Unpublished
master's thesis). University of Lisbon - Department of Psychology,
Portugal.




# Fellowship

-----------------------------------------------------------------------

PhD scholarship: PD/BD/135465/2017 Funded by Fundação para a Ciência e
Tecnologia (FCT)




# Teaching

-----------------------------------------------------------------------



## Bachelors

2024: Statistical Analysis II, Undergraduate Course at ISPA
(Coordinator: Nuno Gomes) - taught two cohorts, three hours a week each,
for a semester

2024: Philosophy of Science, Undergraduate Course at ISPA, taught all
enrolled students, one hour and half a week, for a semester

2024: Learning and Behavior, Undergraduate Course at ISPA, taught the
initial cohort of students to attend the course course, three hours a
week for a semester

2024: Statistical Analysis I, Undergraduate Course at ISPA
(Coordinator: Nuno Gomes Roberto) - taught three classes of introduction
to R to the initial cohort

2024: Statistics Applied to Psychology, Undergraduate Course at FPUL
(Coordinator: Ana Luísa Raposo) - Taught three modules (lectures and
sectionals) to all enrolled students

2024: Data Analysis I, Undergraduate Course at ISPA
(Coordinator: Gonçalo Aires de Oliveira) - taught two cohorts, three hours a
week each, for a semester

2023: Philosophy of Science, Undergraduate Course at ISPA, taught the
initial cohort of students to attend the course course), one hour and
half a week, for a semester

2023: Statistical Analysis II, Undergraduate Course at ISPA
(Coordinator: Nuno Gomes Roberto) - taught two cohorts, three hours a
week each, for a semester

2023: Introduction to Probabilities and Statistics Applied to
Psychology, Undergraduate Course at FPUL (Coordinator: Maga Roberto) -
taught four cohorts (sectional classes), two hours a week each, for a
semester

2022: Social Psychology II, Undergraduate Course at FPUL (Coordinator:
Ana Sofia Santos)

2021: Research Methods in Psychology: Basic Notions, Undergraduate
Course at FPUL (Coordinator: Mário Boto-Ferreira)

2021: Social Psychology II, Undergraduate Course at FPUL (Coordinator:
Ana Sofia Santos)

2020: Research Methods in Psychology: Basic Notions, Undergraduate
Course at FPUL (Coordinator: Mário Boto-Ferreira)

2019: Social Psychology I, Undergraduate Course at FPUL (Coordinator:
André Mata)

2018: Themes of Social Psychology, Undergraduate Course at FPUL
(Coordinator: André Mata)



## Masters

2024: Experimental Design and Advanced Ecological Data Analysis,
Graduate Level Course, for the Masters Program in Marine and
Conservation Biology, at ISPA, three hours a week for a semester

2024: Graduate Seminar in Social and Cognitive Psychology, at FPUL
(Coordinator: Tomás Palma) - Taught a two hour module on open science

2022: Graduate Seminar in Social and Cognitive Psychology, at FPUL
(Coordinator: André Mata)


## PhD

2021: Research Methodology, Graduate Seminar at FPUL (Coordinator: Magda
Roberto)

2018: Research Methodology, Graduate Seminar at FPUL (Coordinator: Magda
Roberto)




# Workshops

-----------------------------------------------------------------------

2023: SciOps: Next-Level OpenScience?. Workshop presented at the QHELP
2023 Mobility event, Arrifana, Portugal

2023: `git` and GitLab. Workshop presented at the QHELP 2023 Mobility
event, Arrifana, Portugal

2022: Openness & Community: Catalysts for Quality Ethical Science.
Workshop presented at a QHELP Multiplier event, at the University of
Lisbon - Department of Psychology (Co-author: Inês Ramalhete)

2021: Modelos Multinível no R: Uma Missão ~~Im~~Possível. Invited
Workshop at the University of Lisbon - Department of Psychology.

2020: O que é e como se faz a ciência? Dicas para produtores e
consumidores. Workshop presented at the December Workshops at the
University of Lisbon - Department of Psychology

2020: Um novo robô de cozinha: o R! Utilização e receitas básicas.
Workshop presented at the December Workshops at the University of
Lisbon - Department of Psychology

2017: Workshop on mouse-tracking, Invited Workshop at the University of
Lisbon - Department of Psychology (Main author: Sara Hagá)




# Manuscripts

-----------------------------------------------------------------------

Hagá, S., & Santos, J. O. (in prep). *Regarding children as less human
than adults.*

Santos, J. O., Hara, T., Do Bú, E. A., Mendonça, C., Hagá, S., Pogacar,
R., & Kouril, M. (2023). *Implicit Association Tests for All: Using
iatgen for Non-English and Offline Samples.* [Manuscript submitted for
publication]

Hagá, S., & Santos, J. O. (in prep). *Humanness and Valence Norms for
Portuguese Words.*




# Projects

-----------------------------------------------------------------------

2024-Present: Maintainer of the
[`tr.iatgen` `R` package](https://github.com/iatgen/tr.iatgen)
(also contributed to the
[`iatgen` R package](https://github.com/iatgen/iatgen)).

2022-Present: Helped establish and coordinate an `R` user group named
[RUGGED](https://rggd.gitlab.io). I'm also spearheading the development
of the [group's projects](https://rggd.gitlab.io/projects.html), namely:

- MOC: An ongoing attempt at building a *Medium Online Course* on using
  `R` for data analysis in psychology and related fields. The unfinished
  draft is available [here](https://rggd.gitlab.io/moc).

- SciOps: An ongoing attempt to create a template (or templates) to help
  researchers leverage cutting-edge computer tools to increase their
  productivity, by automating repetitive tasks, as well as their
  research's reproducibility and transparency.

- RUGGED's website: The group's official
  [website](https://rggd.gitlab.io).

2021-Present: SPGrow 360 – collaborated with Sérgio Moreira in
developing an R/shiny application for the automatic generation of 360
(i.e., self- and peer-evaluation) evaluation reports of workers' social
performance metrics.

2022: QHELP - worked with and mentored students, under the supervision
of Sérgio Moreira, to build `R/Shiny` apps for teaching about linear
models and their assumptions.

2017-2021: Collaborated in developing and maintaining LiSP’s PhD program
official website.

**Note: To see a more detailed list of my projects please checkout my
[portfolio](https://joao-o-santos.gitlab.io/portfolio.html).**




# Oral presentations

-----------------------------------------------------------------------

Santos, J. O., Hagá, S., & Garcia-Marques, L. (2023, March-April).
*Questions, Answers, Problems, and Suggestions For the Replication
Crisis.* 17º Encontro da Associação de Portuguesa de Psicologia
Experimental (APPE), FP-UL, Lisbon, Portugal.

Santos, J. O., Hagá, S., Ricoca-Peixoto, M., & Garcia-Marques, L. (2022,
June). *Childism: Idadismo dirigido a crianças.* SNIP XI Simpósio
Nacional de Investigação em Psicologia, Universidade de Trás-Os-Montes
e Alto Douro, Vila Real, Portugal.

Santos, J. O., Garcia-Marques, L., & Hagá, S. (2022, March). *How
Philosophy of Science Can Help Us Sort Out the Replication Crisis.*
JIP22, FP-UL, Lisbon, Portugal.

Santos, J. O., Hagá, S., Garcia-Marques, L., & Dunham, Y. (2021,
September). *What and how do we think of children? Are we discriminating
them?.* ESCON 2021 Transfer of Knowledge Conference, University of
Salzburg, Salzburg, Austria (Online).

Santos, J. O., Garcia-Marques, L., & Hagá, S. (2021, May). *Sorting Out
the Replication Crisis with Philosophy of Science.* XVI PhD Meeting in
Psychology, ISCTE-IUL, Lisbon, Portugal (Online).

Santos, J. O., Hagá, S., & Garcia-Marques, L. (2020, February).
*Innocent until proven child? Adults expect children to commit more
mishaps than adults, but can restrain these expectations.* To be
presented at the International APPE-SEPEX Meeting, University of
Algarve, Portugal. (Conference canceled).

Hagá, S. & Santos, J. O. (2020, June--July). *Perceiving Children as Less
Human Than Adults.* To be presented at the 19th General Meeting of the
EASP, University of Kraków, Poland. (Conference canceled).

Santos, J. O., Hagá, S., Dunham, Y., & Garcia-Marques, L. (2020,
June--July). *What is acceptable? An age old question...quantitative and
qualitative evidence of discrimination against children.* To be
presented at the 19th General Meeting of the EASP, University of Kraków,
Poland. (Conference canceled).

Hagá, S., & Santos, J. O., (2019, September). *Of mice and boys:
Perceiving children as less human than adults.* 21st ESCON Transfer of
Knowledge Conference, University of Bordeaux, Bordeaux, France

Santos, J. O., Hagá, S., & Garcia-Marques, L. (2019, May). *How do we
stereotype and feel about children and adults?* XV Phd Meeting in Social
and Organizational Psychology, ISCTE-IUL, Lisbon, Portugal.

Santos, J. O., & Hagá, S. (2018, May). *We need to talk...about
childism: Perceived acceptability of discriminatory statements targeting
children.* XIV Phd Meeting in Social and Organizational Psychology,
ISCTE-IUL, Lisbon, Portugal.

Hagá, S., & Santos, J. O. (2018, April). *Perceiving children as less
human than adults.* 13º Encontro da Associação de Portuguesa de
Psicologia Experimental (APPE), Braga, Portugal.

Santos, J. O. & Hagá, S. (2017, March). *Do we infrahumanize our
children?.* Dia do Jovem Investigador em Psicologia (DJIP), FP-UL,
Lisbon, Portugal.




# Poster presentations

-----------------------------------------------------------------------

Santos, J. O., Hagá, S., & Garcia-Marques, L. (2021, April). *Innocent
until proven child? Adults' assumptions about children's and adults'
behaviors.* XV Encontro da Associação de Portuguesa de Psicologia
Experimental (APPE) , ISPA, Lisbon, Portugal (Online).

Santos, J. O., Hagá, S., & Garcia-Marques, L. (2019, May). *Children are
curious and adults are responsible: Stereotypes and feelings towards
children and adults.* 14º Encontro da Associação de Portuguesa de
Psicologia Experimental (APPE), FPCE-UÉvora, Évora, Portugal.

Ricoca-Peixoto, M., Hagá, S., Santos, J. O., & Garrido, M. P., (2019,
May). *Would you rather meet a childlike adult or an adultlike child?
Childlike and adultlike as attributes in person perception.* XIV Phd
Meeting in Social and Organizational Psychology, ISCTE-IUL, Lisbon,
Portugal.

Ricoca-Peixoto, M., Hagá, S., Santos, J. O., & Garrido, M. P., (2019,
May). *A criança e o adulto dentro de nós, vistos de fora: Percepção de
pessoas descritas como "muito crianças" ou "muito adultas".* 14º
Encontro da Associação de Portuguesa de Psicologia Experimental (APPE),
FPCE-UÉvora, Évora, Portugal.

Santos, J. O., Hagá, S., & Garcia-Marques, L. (2018, June). *Childism is
no child's play: Experimental and anecdotal evidence of discrimination
against children.* Future of Social Cognition workshop, East Anglia,
England.

Santos, J. O., & Hagá, S. (2018, April). *While you're young it's
acceptable: Perceived acceptability of discriminatory statements
targeting children.* 13º Encontro da Associação de Portuguesa de
Psicologia Experimental (APPE), Braga, Portugal.

Santos, J. O., & Hagá, S. (2017, June). *Are children perceived as less
human than adults?.* XIII Phd Meeting in Social and Organizational
Psychology, Lisbon, Portugal.

Santos, J. O., & Hagá, S. (2017, May). *Wild children and civilized
adults: Studies on the infrahumanization of children.* 12º Encontro da
Associação de Portuguesa de Psicologia Experimental (APPE), Oporto,
Portugal.




# Professional memberships

-----------------------------------------------------------------------

Associação Portuguesa de Psicologia Experimental (APPE) - Member since
2018




# Award

-----------------------------------------------------------------------

Jornadas do Jovem Investigador na Faculdade de Psicologia 2022 – **2nd
Place** - best conference presentation



# Additional Training

-----------------------------------------------------------------------

2022: QHelp, one week intensive seminar in quantitative thinking,
statistics (including R and Shiny apps) at KU Leuven (Katholieke
Universiteit Leuven)

2019-2021: WriteOn, Scientific Writing Workshop at the University of
Lisbon - Department of Psychology

2019: Analyzing discourse and communication for understanding
socio-psychological processes at ISCTE-IUL

2018: X Summer School In Advanced Methods Of Data Analysis, at the
University of Lisbon - Department of Social Sciences (ICS-UL)

- Introduction to Quantitative Analysis

- Multivariate Analysis (in R) at the University of Lisbon

- Structural Equation Modeling (in R)

2018: Introduction to E-Prime: From the design of the experiment to data
management and processing at ISCTE-IUL

2018: Qualitative data analysis with NVivo 11 at ISCTE-IUL

2018: Systematic literature review and meta-analysis in Psychology at
ISCTE-IUL

2018: Moderated Mediation at ISCTE-IUL

2018: Publishing empirical articles that have information value at
ISCTE-IUL

2017: Techniques of Data Analysis I at ISPA




# Skills

-----------------------------------------------------------------------

Statistical analyzes: `R` (including: mixed effects and robust
models, `ggplot2`), `JASP`, `SPSS`.

Experiment Programming: `Qualtrics`, `Iatgen`, `OpenSesame`, `Mouse
Tracker`, `E-PRIME`.

Open Science: Preregistration and data sharing using
[osf.io](https://osf.io) and [aspredicted.org](https://aspredicted.org).

Markup: `Markdown` (including `RMarkdown` and `Bookdown`), `LaTeX`, as
well as multi-format conversion using `pandoc` (including custom
templates).

Programming: `C`, `R`, `POSIX shell`, also dabbled in `Python`, `Lua`
and `SQL`.

Web: `html`, `css`, interactive `R/Shiny` applications.

Tools: `Linux` (proficient user), `git`, `make`, `sed` as well as other
`coreutils`; `GitHub` and `Gitlab` (including `gitlab CI/CD`).




# Languages

-----------------------------------------------------------------------

Portuguese (native speaker)

English (fluent)

French (basic comprehension)

Spanish (basic comprehension)

Latin (very basic comprehension, personal hobby)
