# SPDX-License-Identifier: CC0-1.0

include config.mk

all: clean pages pdf

pdf:
	${BROWSER} ${BFLAGS} --print-to-pdf=public/cv.pdf public/index.html
	${BROWSER} ${BFLAGS} --print-to-pdf=public/short.pdf public/short.html
	${BROWSER} ${BFLAGS} --print-to-pdf=public/teaching.pdf public/teaching.html

pages: standard teaching short

standard:
	cat header.html > public/index.html
	markdown cv.md >> public/index.html
	cat footer.html >> public/index.html

teaching:
	cat teaching_header.html > public/teaching.html
	markdown teaching.md >> public/teaching.html
	cat footer.html >> public/teaching.html

short:
	cat short_header.html > public/short.html
	markdown short.md >> public/short.html
	cat footer.html >> public/short.html

clean:
	-rm public/index.html
	-rm public/cv.pdf
